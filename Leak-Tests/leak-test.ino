#include <LowPower.h>
#include <Wire.h>
#include <SPI.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_BMP3XX.h>
#include <SD.h>

#define SEALEVELPRESSURE_HPA (1013.25)

// SD card pin configuration
const int chipSelect = 10; // Adjust this to your setup

Adafruit_BMP3XX bmp;

void setup() {
  Serial.begin(115200);
  while (!Serial);

  // Initialize the BMP sensor
  if (!bmp.begin_I2C()) {
    Serial.println("Could not find a valid BMP3 sensor, check wiring!");
    while (1);
  }

  // Set up oversampling and filter initialization
  bmp.setPressureOversampling(BMP3_OVERSAMPLING_4X);
  bmp.setIIRFilterCoeff(BMP3_IIR_FILTER_COEFF_3);
  bmp.setOutputDataRate(BMP3_ODR_50_HZ);

  // Initialize the SD card
  if (!SD.begin(chipSelect)) {
    Serial.println("Card failed, or not present.");
    while (1);
  }
  Serial.println("Card initialized.");

  // Create or open the log file
  File dataFile = SD.open("test.txt", FILE_WRITE);
  if (dataFile) {
    dataFile.println("Pressure (Pa)");
    dataFile.close();
  } else {
    Serial.println("Error opening test.txt");
  }
}

void loop() {
  if (!bmp.performReading()) {
    // Serial.println("Failed to perform reading :(");
    return;
  }

  // Readings
  // float pressure = bmp.pressure / 100.0; // Convert Pa to hPa
  float pressure = bmp.pressure; // Pressure in Pa 

  // Print readings to Serial Monitor
  // Serial.print("Pressure = ");
  // Serial.print(pressure);
  // Serial.println(" Pa");

  // Write readings to SD card
  File dataFile = SD.open("test.txt", FILE_WRITE);
  if (dataFile) {
    dataFile.println(pressure);
    dataFile.close();
  } else {
    // Serial.println("Error opening test.txt");
  }

  // Serial.println();
   
  // delay(500);
  for (int i = 0; i < 50; i++) {
    LowPower.powerDown(SLEEP_8S, ADC_OFF, BOD_OFF);
  }
  // delay(500);  
}
